# OpenML dataset: League-of-Legends-Diamond-Games-(First-15-Minutes)

https://www.openml.org/d/43635

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Inspired by the following dataset , we have a collection of data on the first 15 minutes of about 50000 Diamond ranked League of Legends matches scraped using Riot's API. 
Can you predict their outcomes?
Content
Data
All matches were collected with the following parameters:
Season: 13
Server: NA1
Rank: Diamond
Tier: I,II,III,IV
Acknowledgements
Thank you to Riot Games for allowing access to their API.
Inspiration
When working on the linked dataset above, we see classification accuracy peak around 70. Given that we have 5 times the amount of data, I wanted to explore how this would improve our results.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43635) of an [OpenML dataset](https://www.openml.org/d/43635). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43635/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43635/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43635/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

